module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        critical: {
            test: {
                options: {
                    base: './',
                    css: [
                        'src/e2/rv5_css/women/style.css'
                    ],
                    dimensions: [{
                        height: 768,
                        width: 1366
                    }, {
                        height: 640,
                        width: 360
                    }, {
                        height: 568,
                        width: 320
                    }]
                },
                src: 'src/_site/index.html',
                dest: 'src/_site/critical.html'
            }
        },
        jslint: {
            client: {
                src: [
                    'src/_js/**/*.js'
                ],
                directives: {
                    browser: true
                },
                options: {

                }
            }
        },
        jshint: {
            all: [
                'src/_js/**/*.js'
            ]
        },
        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'src/e2/rv5_css/women/style.css': 'src/_scss/women/style.scss',
                    'src/e2/rv5_css/women/old_ie_style.css': 'src/_scss/women/old_ie_style.scss'
                }

            }
        },
        watch: {
            scripts: {
                files: ['src/_js/*/*.js'],
                tasks: ['uglify'],
                options: {
                    spawn: false
                }
            }
        },
        jsbeautifier: {
            files: ['src/**/*.html'],
            options: {}
        },
        accessibility: {
            options: {
                accessibilityLevel: 'WCAG2A'
            },
            test: {
                src: ['src/**/*.html']
            }
        },
        curl: {
            'build/rv5_images.zip': 'https://bitbucket.org/armydotmil/<%= pkg.name %>/downloads/rv5_images.zip',
            'build/rv5_downloads.zip': 'https://bitbucket.org/armydotmil/<%= pkg.name %>/downloads/rv5_downloads.zip'
        },
        zip: {
            'build/rv5_images.zip': ['src/e2/rv5_images/**/*'],
            'build/rv5_downloads.zip': ['src/e2/rv5_downloads/**/*']
        },
        unzip: {
            highlight: {
                src: ['build/rv5_images.zip', 'build/rv5_downloads.zip'],
                dest: 'unzipped/'
            }
        },
        replace: {
            cdn: {
                src: ['src/**/*.html', 'src/_js/**/*.js', 'src/_scss/**/*.scss', 'src/_data/**/*.yml'],
                overwrite: true,
                replacements: [{
                    from: 'http://frontend.ardev.us/development/<%= pkg.name %>/_site/e2/',
                    to: '/e2/'
                }, {
                    from: 'http://frontend.ardev.us/api/',
                    to: 'http://www.army.mil/api/'
                }]
            },
            dev: {
                src: ['src/**/*.html', 'src/_js/**/*.js', 'src/_scss/**/*.scss', 'src/_data/**/*.yml'],
                overwrite: true,
                replacements: [{
                    from: /\/e2\/(?!rv5_js\/3rdparty|rv5_js\/main)/g,
                    to: 'http://frontend.ardev.us/development/<%= pkg.name %>/_site/e2/'
                }, {
                    from: 'http://www.army.mil/api/',
                    to: 'http://frontend.ardev.us/api/'
                }]
            }
        },
        'http-server': {
            'dev': {
                root: 'src/',
                port: 8282,
                host: "0.0.0.0",
                ext: "html",
                runInBackground: false
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                mangle: true,
                compress: true,
                beautify: false
            },
            build: {
                files: [
                    {
                        src: [
                            'src/_js/women/fastclick.js',
                            'src/_js/women/spin.min.js',
                            'src/_js/women/jquery.unveil.js',
                            'src/_js/women/w.js'
                        ],
                        dest: 'src/e2/rv5_js/women/<%= pkg.name %>.min.js'
                    },
                    {
                        src: [
                            'src/_js/women/owl.carousel.min.js',
                            'src/_js/women/owlloader.js'
                        ],
                        dest: 'src/e2/rv5_js/women/owl-custom.min.js'
                    },
                    {
                        src: [
                            'src/_js/women/underscore-min.js',
                            'src/_js/women/backbone-min.js',
                            'src/_js/women/youtube_iframe_api.js',
                            'src/_js/women/youtube_player.js',
                            'src/_js/women/article.js'
                        ],
                        dest: 'src/e2/rv5_js/women/<%= pkg.name %>.news.min.js'
                    }
                ]
            }
        },
        'sftp-deploy': {
            build: {
                auth: {
                    host: 'frontend.ardev.us',
                    authKey: 'privateKey'
                },
                cache: 'sftpCache.json',
                src: 'src/',
                dest: '/www/development/<%= pkg.name %>',
                exclusions: ['build/', 'node_module/', 'Gruntfile.js', 'package.json', 'readme.md', '.sass-cache', '.git', '.gitignore'],
                serverSep: '/',
                concurrency: 4,
                progress: true
            }
        },
        bump: {
            options: {
                files: ['package.json'],
                updateConfigs: [],
                commit: true,
                commitMessage: 'Release v%VERSION%',
                commitFiles: ['package.json'],
                createTag: true,
                tagName: 'v%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: false,
                gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
                globalReplace: false,
                prereleaseName: false,
                regExp: false
            }
        }
    });

    grunt.loadNpmTasks('grunt-bump');

    grunt.loadNpmTasks('grunt-critical');

    grunt.loadNpmTasks('grunt-sftp-deploy');

    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.loadNpmTasks('grunt-http-server');

    grunt.loadNpmTasks('grunt-text-replace');

    grunt.loadNpmTasks('grunt-curl');

    grunt.loadNpmTasks('grunt-zip');

    grunt.loadNpmTasks('grunt-accessibility');

    grunt.loadNpmTasks('grunt-jslint');

    grunt.loadNpmTasks("grunt-jsbeautifier");

    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.loadNpmTasks('grunt-contrib-sass');

    grunt.registerTask('default', ['sass', 'uglify']);

    grunt.registerTask('dev', ['replace:dev', 'sass', 'uglify']);

    grunt.registerTask('cdn', ['replace:cdn', 'sass', 'uglify']);

    grunt.registerTask('images', ['curl', 'unzip']);

};
