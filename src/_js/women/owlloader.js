/*global $,document,setTimeout*/
$(window).load(function() {
    $('.owl-carousel').each(function() {
        var sliderItems = $(this).find('.slide'),
            objRef = $(this),
            wrapper,
            topMarg,
            numItems = 3;
        if (sliderItems.length < 3) numItems = sliderItems.length;
        $(this).owlCarousel({
            // Most important owl features
            items : numItems,
            responsive: true,
            itemsDesktop : false,
            itemsDesktopSmall : [992,numItems],
            itemsTablet: [980,2],
            itemsTabletSmall: false,
            itemsMobile : [659,1],
            navigation: true,
            navigationText: ['<span>previous</span>', '<span>next</span>'],
            rewindNav: false,
            beforeUpdate : function() {
                sliderItems.height('auto');
            },
            afterUpdate : function() {
                topMarg = parseInt($(sliderItems).css('margin-top'), 10);
                wrapper = objRef.find('.owl-wrapper');
                sliderItems.height(wrapper.height() - topMarg);
            },
            afterInit : function() {
                setTimeout(function() {
                    topMarg = parseInt($(sliderItems).css('margin-top'), 10);
                    wrapper = objRef.find('.owl-wrapper');
                    sliderItems.height(wrapper.height() - topMarg);
                }, 100);
            }
        });
    });

    $('.slider-container .flip-btn').click(function() {
        $(this).closest('.slide').toggleClass('flipped');
    });
});