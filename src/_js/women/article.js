/*global $,document,Backbone,window*/

var keyword = 'target_women';

var Article = Backbone.Model.extend({
    urlRoot: 'https://www.army.mil/api/packages/getpackagesbykeywords?keywords=' + keyword
});

var Articles = Backbone.Collection.extend({
    model: Article,
    url: 'https://www.army.mil/api/packages/getpackagesbykeywords?keywords=' + keyword
});

var articles = new Articles();

var ArticleGallery = Backbone.View.extend({
    initialize: function(options) {
        for (var i = 0; i < options.rows; i++) {
            this.listenTo(articles, 'sync', this.load_articles);
        }
        articles.url += '&offset=' + options.offset + '&count=' + options.count;
        articles.fetch();
    },
    articles_loaded: 0,
    load_articles: function() {
        var i = 0,
        news_stories = $('<div>').prop('class', 'item-wrap'),
        num_articles = 3;

        for (i; i < num_articles; i++) {
            if (articles.length > 0) {
                news_stories.append(build_news_cell(articles.shift(), i, this.articles_loaded));
                this.articles_loaded++;
            } else {
                $('#more-news').hide();
            }
        }
        $('#army-news .button-wrap').before(news_stories);
    }
});

function build_news_cell(article, pos, articles_loaded) {
    var news_story = $('<div>').prop({
        'class': 'item'
    }),
    max_index = article.get('images').length - 1,
    random_index = Math.floor((Math.random() * max_index) + 0),
    image = article.get('images')[random_index].url_size1,
    alt = article.get('images')[random_index].alt,
    news_story_image, news_story_title,
    title,
    url = article.get('page_url');

    title = article.get('title');

    news_story_image = $('<a>').prop({
        'href': url,
        'target': '_blank',
        'class': 'item-img'
    });

    $('<img>').load(function() {
        news_story_image.append($(this));
    }).prop({
        'src': image,
        'alt': alt,
        'class': pos === 0 && articles_loaded === 0 ? 'top-img' : ''
    });

    news_story_title = $('<a>').prop({
        'href': url,
        'target': '_blank'
    }).text(title);

    return news_story.append(
        news_story_image,
        news_story_title
    );
}

function resizingPage() {
    var i = 0, items = $('#army-news .item'),
        mh = 0, win = $(window).width();

    $('#army-news .item').height('');
    if (win < 769 && win > 480) {
        for (i; i < items.length; i++) {
            if ($(items[i]).height() > mh) mh = $(items[i]).height();
        }

        $('#army-news .item').height(mh);
    }
}

$(document).ready(function() {

    var articleGallery = new ArticleGallery({
        rows: 2,
        offset: 1,
        count: 100
    }),
    i;

    // bind click function to load more button
    $('#more-news').click(function(e) {
        e.preventDefault();
        articleGallery.articles_loaded++;
        for (i = 0; i < 2; i++) {
            articleGallery.load_articles();
        }
        resizingPage();
    });
});

$(window).load(resizingPage)
.resize(resizingPage);



