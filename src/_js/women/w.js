/*global $,window,document,Modernizr,FastClick,setTimeout,Image,Spinner*/

function Women() {
    'use strict';

    var _instance = this,
        $body = $('body');

    _instance.position = $(window).scrollTop();
    _instance.scroll_page = 'window';

    // Check for touch
    if (Modernizr.touch) {
        $body.on('touchmove', function() {
            _instance.dragging = true;
        })
        .on('touchstart', function() {
            _instance.dragging = false;
        });
    }

    _instance.events();

    // initialize fast click plugin
    FastClick.attach(document.body);

    return _instance;
}

// not in use but could come in handy
Women.prototype.get = function() {
    return this || new Women();
};


Women.prototype.lightbox = {};
Women.prototype.lightbox.image = {};
Women.prototype.menu = {};

Women.prototype.lightbox.init = function(elem) {
    var lb_content, p_img, p_name, p_rank, p_subtitle;
    $('#lightbox').addClass('is-visible');
    $('html, body').addClass('menu-open');

    p_rank = elem.find('p.accent-font').text();
    p_name = elem.find('h4').text();
    p_img = elem.find('img').prop('src');
    p_subtitle = elem.find('.profile-subtitle').text();
    lb_content = elem.find('.lb-content').html();

    p_img = p_img.match('320') ? p_img.replace('320', '460') : p_img;

    $('#lightbox .header').empty().append(
        $('<h5>').text(p_rank),
        $('<h3>').text(p_name)
    );

    $('#lightbox .inner-container').empty().append(
        $('<img>').prop('src', p_img),
        $('<p class="lead-p">').text(p_subtitle),
        lb_content,
        $('<div class="clearfix">')
    );

    $('#lightbox .container').addClass('fade-in-up');

    $('#lightbox .inner-container a').prop('target', '_blank');
};

Women.prototype.lightbox.spotlight = function(elem) {
    var lb_content, c_img, c_name;
    $('#lightbox').addClass('is-visible');
    $('#lightbox .spotlight').show();
    $('html, body').addClass('menu-open');

    c_name = elem.find('h4').text();
    c_img = elem.find('img').prop('src');
    lb_content = elem.find('.lb-content').html();

    $('#lightbox .spotlight .header').empty().append(
        $('<h5>').text('SPOTLIGHT CONTRIBUTOR'),
        $('<h3>').text(c_name)
    );

    $('#lightbox .spotlight .inner-container').empty().append(
        $('<img />').prop('src', c_img),
        lb_content,
        $('<div class="clearfix">')
    );

    $('#lightbox .spotlight .inner-container p:first').addClass('lead-p');

    $('#lightbox .spotlight.container').addClass('fade-in-up');

    $('#lightbox .spotlight .inner-container a').prop('target', '_blank');
};

Women.prototype.lightbox.destroy = function() {
    $('#lightbox .container').removeClass('fade-in-up').addClass('fade-out-up');
    setTimeout(function() {
        $('#lightbox .container').removeClass('fade-out-up');
        $('#lightbox').removeClass('is-visible image-lb');
        $('#lightbox .spotlight').hide();
        $('html, body').removeClass('menu-open');
    }, 700);
};

/*
 * Set up the click listner on the appropriate images
 */
Women.prototype.lightbox.image.init = function() {
    var $this = this,
        spinopts = {
            color: '#fff',
            shadow: true
        };

    this.index = 0;
    this.start_w = $(window).width();
    this.images = $('.image-mosaic .image-tile a, .roosevelt-bill');
    this.len = this.images.length;

    if (typeof this.spinner === 'undefined') {
        this.loader = $('#lightbox .image-loading');
        this.spinner = new Spinner(spinopts).spin();
        this.loader.append(this.spinner.el);
    }

    this.images.on('click', function() {
        $this.onClick($(this));
    });
};

/**
 * Destroy lightbox function
 */
Women.prototype.lightbox.image.destroy = function() {
    $('#lightbox').removeClass('is-visible image-lb');
    $('#lightbox .no-bg').hide();
    $('html, body').css('overflow', '');

    $('.lb-img p').html('');

    this.index = 0;
};

/**
 * Click event function
 * @param {element} elem
 */
Women.prototype.lightbox.image.onClick = function(elem) {
    var $this = this;
    $this.img = $(elem).find('img');
    $this.alt_text = $this.img.prop('alt');
    $this.index = $this.images.index($(elem));

    $('html, body').css('overflow', 'hidden');
    $('#lightbox').addClass('is-visible image-lb');
    $('#lightbox .no-bg').show();

    $this.description = $('#lightbox .no-bg .inner-container p');
    $this.lightBoxImage = $('.lb-img img');

    $this.setDisplay($this.images.eq($this.index));
};

/**
 * Function to set the display to the next image
 */
Women.prototype.lightbox.image.goRight = function() {
    var $this = this;

    if ($this.index < $this.len - 1) $this.index++;

    $this.setDisplay($this.images.eq($this.index));
};

/**
 * Function to set the display to the previous image
 */
Women.prototype.lightbox.image.goLeft = function() {
    var $this = this;

    if ($this.index > 0) $this.index--;

    $this.setDisplay($this.images.eq($this.index));
};

/**
 * Update the lightbox with the
 * appropiate content
 * @param {element} elem
 */
Women.prototype.lightbox.image.setDisplay = function(elem) {
    var newImage = new Image(),
        $this = this;

    if (this.index === 0) {
        $('.lb-button').show();
        $('.lb-button#prev').hide();
    } else if (this.index === this.len - 1) {
        $('.lb-button').show();
        $('.lb-button#next').hide();
    } else {
        $('.lb-button').show();
    }

    this.img = elem.find('img');
    this.alt_text = this.img.prop('alt');
    this.description.text(this.alt_text);

    this.loader.show();
    newImage.onload = function() {
        $this.loader.hide();
        $this.lightBoxImage.prop({
            'alt': $this.alt_text,
            'src': this.src
        });
    };
    newImage.src = this.img.prop('src').replace('thumbnail', 'full');
};

Women.prototype.menu.show = function() {
    $('html, body').toggleClass('menu-open');
    $('header .header-container div').toggleClass('open');
};

Women.prototype.events = function() {
    var $this = this,
        currentSubpageURL = '';

    $('.hamburger').on('click', function() {
        if ($this.dragging) return;
        $(this).toggleClass('active');
        $this.menu.show();
        return false;
    });

    $('#lightbox').on('click', function(e) {
        var target = e.target || e.srcElement; // for older IE
        if ($this.dragging) return;
        
        if ($(target).hasClass('click-close')) {
            if ($('.fade-in-up').length) {
                $this.lightbox.destroy();
            } else {
                $this.lightbox.image.destroy();
            }
        }
        if (target.tagName !== 'A') return false;
    });

    // on click right
    $('.lb-button#next').on('click', 'a', function() {
        $this.lightbox.image.goRight();
    });

    // on click left
    $('.lb-button#prev').on('click', 'a', function() {
        $this.lightbox.image.goLeft();
    });

    $(document).keydown(function(e) {
        if ($('#lightbox').is(':visible')) {
            if (e.keyCode === 27) {

                if ($('#profiles-list').length) {
                    $this.lightbox.destroy();
                } else {
                    $this.lightbox.image.destroy();
                }

            // 37 = left
            } else if (e.keyCode === 37) {
                $this.lightbox.image.goLeft();

            // 39 = right
            } else if (e.keyCode === 39) {
                $this.lightbox.image.goRight();
            }
        }
    });

    $('.social').on('click', 'a', function() {
        window.open(
            this.href,
            '',
            'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'
        );
        return false;
    });
    if ($('#history-page').length) {
        $('html').on('click', function() {
            $('nav').removeClass('open');
        });
        $('nav').on('click', function(e) {
            e.stopPropagation();
        });
    }

    $('nav').on('click', 'li a', function(e) {
        if ($this.dragging) return;
        e.preventDefault();
        var margin = 70, offset = 130,
            target = $(this.hash),
            win = $(window).width();

        target = target.length ?
            target :
            $('[name=' + this.hash.slice(1) + ']');

        if (target.length) {

            /**
             * to compensate for the header bar
             * header bar with both navs height = 130
             * header bar with only sub nav height = 60
             */
            if (win < 769) {
                margin = win < 481 ? 50 : 60;
                offset = target.offset().top < $this.position ? offset : 0;
            }
            if ($('#intro').length) margin = 0;
            $('html, body').animate({
                scrollTop: target.offset().top - offset - margin
            }, 1000);

            $('nav').removeClass('open');
            $('html,body').removeClass('menu-open');

            return false;
        } else if ($('#profiles-list').length) {
            if ($(this).hasClass('current-toggle')) {
                $('.current').show();
                $('.veteran').hide();
            } else {
                $('.current').hide();
                $('.veteran').show();
            }
            $('nav li').removeClass('active');
            $(this).parents('li').addClass('active');
        } else if ($(this).hasClass('show-menu')) {
            $('nav').toggleClass('open');

            if (win < 769 &&
                $this.position > $('header').height() &&
                $('nav').hasClass('open')) {
                $('html,body').addClass('menu-open');
            } else {
                $('html,body').removeClass('menu-open');
            }
            return false;
        }
    });

    $('.profile').on('click', 'h6 a', function() {
        $this.lightbox.init($(this).parents('.profile'));
    });

    $('.contributor').on('click', 'h6 a', function() {
        $this.lightbox.spotlight($(this).parents('.contributor'));
    });

    $('.read-more-section').on('click', '.read-more-button a', function() {
        var section = $(this).parents('.read-more-section'),
            section_imgs = section.find('img');

        if (section_imgs.length) {
            section_imgs.unveil();
            $this.lightbox.image.images = $('.readmore-image .image-tile a, .integration-act', section);
            $this.lightbox.image.images.on('click', function() {
                $this.lightbox.image.images = $('.readmore-image .image-tile a, .integration-act', section);
                $this.lightbox.image.len = $this.lightbox.image.images.length;
                $this.lightbox.image.onClick($(this));
            });
        }

        if (section.hasClass('show-more')) {

            // closing section
            $('html, body').animate({
                scrollTop: section.offset().top - 200
            }, 300);
            $this.lightbox.image.images.off('click');
        }
        section.toggleClass('show-more');
    });

    $('.open-sub-page').on('click', 'a', function(e) {
        var url = $(this).attr('href').replace('../', '../ajax_content/');

        e.preventDefault();

        if (url !== currentSubpageURL) {
            $('.sub-page-container .container').load(url, function() {
                $('body').addClass('show-sub-page');
                $('.sub-page-container').scrollTop(0);
                $this.lightbox.image.init();
            });
            currentSubpageURL = url;
        } else {
            $('body').addClass('show-sub-page');
            $this.lightbox.image.init();
        }
        $('header').removeClass('hidden');
    });

    $('.history-back-link').on('click', 'a', function(e) {
        if($('body').hasClass('is-sub-page')) return;
        e.preventDefault();
        $('body').removeClass('show-sub-page');
        $('header').removeClass('hidden');
    });
};

(function() {
    var w = new Women();

    if ($('#history-page').length)
        w.lightbox.image.init();

    // initialize the navbars
    $(document).ready(function() {
        $('.slider-container img').unveil(200);
        scrollingPage();
    });

    $('.sub-page-container').scroll(scrollingPage);
    $(window).scroll(scrollingPage);

    function scrollingPage() {
        var bottom_offset, buffer = $('header').height(), divs = [
                $('#quote'),
                $('#army-news'),
                $('#army-videos'),
                $('#army-resources')
            ], i,
            nav_offset = $('.masthead-container .full-width-image').height(),
            scroll_top = $(this).scrollTop(),
            win = $(this).width(),
            page_scrolling = 'window';

        if ($(this).hasClass('sub-page-container')) {
            page_scrolling = 'subpage';
            nav_offset = 0;
        }

        if (page_scrolling !== w.scroll_page) {
            w.position = scroll_top;
            w.scroll_page = page_scrolling;
        }

        /*
         * complicated stuff --
         * the sticky class adds position fixed for the secondary nav (top nav is always fixed, top 0)
         * the hidden class hides both navs when scrolling up on tablet/mobile (done with css, not here)
        */
        if (scroll_top >= nav_offset) {
            if (scroll_top === w.position)
                $('header').addClass('sticky');
            // scrolling after the buffer
            if (scroll_top > w.position) {

                // add both classes here to hide both navs
                if ($('nav').hasClass('open') && win < 769) {
                    $('header').addClass('sticky');
                    $('html,body').addClass('menu-open');
                } else if (scroll_top > buffer || win > 768) {
                    $('header').addClass('sticky hidden');

                }
            } else {
                // show them when you scroll up
                $('header').removeClass('hidden');
            }
        } else {
            // scrolling before the buffer
            // if you are scrolling anywhere before the buffer, show both navs
            $('header').removeClass('sticky hidden');
        }

        // put the active state on the appropriate header (on home page)
        if($('#intro').length) {
            for (i = 0; i < divs.length; i++) {
                if (scroll_top >= divs[i].offset().top - 150) {
                    $('nav li').removeClass('active');
                    $('nav li').eq(i).addClass('active');
                }
            }

            bottom_offset = $('.wrapper').height();
            if (($(window).height() + scroll_top) >= bottom_offset) {
                $('nav li').removeClass('active');
                $('nav li:last').addClass('active');
            }
        }

        w.position = scroll_top;
    }

}());







